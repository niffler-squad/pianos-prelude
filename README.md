# Pianos Prelude

## Install

`nvm use`

`npm install`

## Yarn commands

`npm dev`: to start Next.js in development mode.

`npm build`: to build the application for production usage.

`npm start-staging`: to start a Next.js server.

`npm lint`: to set up Next.js' built-in ESLint configuration.
