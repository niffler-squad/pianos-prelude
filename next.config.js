module.exports = {
  reactStrictMode: true,
  images: {
    unoptimized: true,
  },
  async headers() {
    return [
      {
        source: '/(.*)',
        headers: [
          {
            key: 'Strict-Transport-Security',
            value: 'max-age=63072000; includeSubDomains; preload'
          },
          {
            key: 'Content-Security-Policy',
            value:
              "font-src 'self' https://cdnjs.cloudflare.com ;" +
              "img-src 'self' ;" +
              "object-src 'none' ;" +
              "script-src 'self' 'unsafe-eval' https://cdnjs.cloudflare.com ;" +
              "style-src 'self' 'unsafe-inline' https://cdnjs.cloudflare.com ;",
          },
          {
            key: 'X-Frame-Options',
            value: 'SAMEORIGIN',
          },
          {
            key: 'X-Content-Type-Options',
            value: 'nosniff',
          },
          {
            key: 'Referrer-Policy',
            value: 'origin-when-cross-origin',
          },
          {
            key: 'Permissions-Policy',
            value: 'camera=(), microphone=(), geolocation=(), browsing-topics=()'
          },
        ],
      },
    ];
  },
}
