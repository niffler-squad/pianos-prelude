import React from "react";

function Arrow(props: { position?: any; className?: any; style?: any; onClick?: any; }) {
    const {className, style, onClick} = props;
    return (
        <div className={`${className} ${props.position} z-10`} style={style} onClick={onClick}/>
    );
}

export default Arrow;
