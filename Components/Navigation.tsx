import Link from "next/link";
import styles from "../styles/Navigation.module.scss";
import React from "react";
import {Disclosure} from '@headlessui/react'
import {Bars3Icon, XMarkIcon} from '@heroicons/react/24/outline'
import Image from "next/image";

interface ItemProps {
    name: string,
    href: string,
    current: boolean,
}

const NAVIGATION: Array<ItemProps> = [
    {name: 'À propos', href: 'a-propos', current: false},
    {name: 'Services et prestations', href: 'services-et-prestations', current: false},
    {name: 'Pianos en vente', href: 'pianos-en-vente', current: false},
    {name: 'Témoignages', href: 'temoignages', current: false},
    {name: 'Contact', href: 'contact', current: false},
]

interface NavigationProps {
    activeSection: string;
}

export default function Navigation(props: NavigationProps): JSX.Element {

    return (
        <Disclosure as="header" className={`${styles.navigation} sticky top-0 z-20 border-b-2`}>
            {({open}) => (
                <>
                    <nav className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
                        <div className="relative flex h-16 items-center justify-between">

                            <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                                <Disclosure.Button
                                    className="relative inline-flex items-center justify-center rounded-md p-2 focus:outline-none focus:ring-1 focus:ring-inset focus:ring-black">
                                    <span className="absolute -inset-0.5"/>
                                    {open ? (<XMarkIcon className="block h-6 w-6" aria-hidden="true"/>) : (
                                        <Bars3Icon className="block h-6 w-6" aria-hidden="true"/>)}
                                </Disclosure.Button>
                            </div>

                            <div
                                className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-between">

                                <Link href="/#" className="flex">
                                    <div className="flex flex-shrink-0 items-center">
                                        <Image className="h-10 w-auto"
                                               src="/logo.svg" alt="Logo représentant des touches de piano"
                                               width={40} height={40}/>
                                        <p className={`${styles.logo} pl-3 text-md font-medium`}>
                                            Pianos Prélude
                                        </p>
                                    </div>
                                </Link>

                                <div className="hidden sm:ml-6 sm:block">
                                    <ul className="flex space-x-4">
                                        {NAVIGATION.map((item: ItemProps, index: React.Key) => (
                                            <li key={index}
                                                className={props.activeSection === item.href ? styles.active : ''}>
                                                <Link href={`/#${item.href}`}
                                                      className='block rounded-md text-sm font-medium px-3 py-2'>
                                                    {item.name}
                                                </Link>
                                            </li>
                                        ))}
                                    </ul>
                                </div>

                            </div>

                        </div>
                    </nav>

                    <Disclosure.Panel className="sm:hidden">
                        <div className="space-y-1 px-2 pb-3 pt-2">
                            {NAVIGATION.map((item: ItemProps, index: React.Key) => (
                                <Disclosure.Button key={index} as="a" href={`/#${item.href}`}
                                                   className={`${props.activeSection === item.href ? styles.active : ''} block px-3 py-2 text-base font-medium`}>
                                    {item.name}
                                </Disclosure.Button>
                            ))}
                        </div>
                    </Disclosure.Panel>

                </>
            )}
        </Disclosure>
    );
};
