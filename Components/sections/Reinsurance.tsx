import React from "react";
import styles from "../../styles/Reinsurance.module.scss";
import {BiLeaf} from 'react-icons/bi';
import {BsHourglassSplit} from 'react-icons/bs';
import {HiOutlineAcademicCap, HiOutlineUserGroup} from 'react-icons/hi2';

interface ReinsuranceElementProps {
    name: string,
    description: string,
    asterisk?: string,
    icon: any
}

const REINSURANCE_ELEMENTS: Array<ReinsuranceElementProps> = [
    {
        name: 'Expérience et formation',
        description: 'Fort de 10 ans d’expérience et diplômé d’un Brevet des Métiers des Arts ' +
            'option piano où j’ai travaillé au plus près des pianistes qu’ils soient ' +
            'débutants ou professionnels.',
        icon: HiOutlineAcademicCap
    },
    {
        name: 'Un travail minutieux',
        description: 'Un musicien passionné incarne un technicien exigeant. Bénéficiez et ' +
            'profitez d’une écoute attentive à vos besoins et d’un travail soigné.',
        icon: BsHourglassSplit
    },
    {
        name: 'Un réseau élargi',
        description: 'Confrères et consoeurs techniciens dans toute la france, transporteurs, ' +
            'studios d’enregistrements, professeurs, salles de concerts et magasins de ' +
            'pianos...',
        icon: HiOutlineUserGroup
    },
    {
        name: 'Eco-responsables',
        description: 'Mes déplacements se font en transport en commun et en edpm*. De plus, la vente et la remise en ' +
            'état des pianos d’occasions offrent des avantages écologiques et économiques.',
        asterisk: '* Engin de déplacement personnel motorisé',
        icon: BiLeaf
    }
]

const Trusted: React.FC = () => {
    return (
        <section data-section="pourquoi-faire-appel-a-moi"
                 className={`${styles.section} ${styles.reinsurance} px-6 pt-7 pb-9 lg:px-8`}>
            <span className={styles.anchor} id="pourquoi-faire-appel-a-moi"></span>

            <div className="mx-auto max-w-7xl">

                <h2 className="text-center text-lg font-semibold leading-7">
                    Pourquoi faire appel à moi
                </h2>

                <div className="mx-auto mt-10 grid max-w-lg gap-x-8 gap-y-10 lg:grid-cols-4 lg:gap-x-10 lg:mx-0 lg:max-w-none">
                    {REINSURANCE_ELEMENTS.map((element: ReinsuranceElementProps, index: React.Key) => (
                        <div key={index}>
                            <element.icon className="h-10 w-10 mx-auto mb-4"/>
                            <h3 className="text-2xl font-bold leading-9 mb-4 text-center">{element.name}</h3>
                            <p className="text-base leading-7">
                                {element.description}
                                {element.asterisk && (
                                    <>
                                        <br/><i className="text-xs">{element.asterisk}</i>
                                    </>
                                )}
                            </p>
                        </div>
                    ))}
                </div>

            </div>

        </section>
    );
};

export default Trusted;
