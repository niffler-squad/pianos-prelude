import React from "react";
import {Parallax} from 'react-scroll-parallax';
import Image from "next/image";
import styles from "../../styles/ParallaxPhoto.module.scss";

interface ParallaxPhotoProps {
    parallaxSrc: string
}

const ParallaxPhoto: React.FC<ParallaxPhotoProps> = (props: ParallaxPhotoProps) => {
    return (
        <section
            className={`${styles.section} ${styles.parallaxPhotoSection} relative isolate overflow-hidden`}>
            <Parallax translateY={[-20, 20]}>
                <figure className="flex items-center justify-center">
                    <Image
                        className="absolute w-full object-center"
                        src={props.parallaxSrc} alt="Photo décorative" width={3000} height={432} priority={true}/>
                </figure>
            </Parallax>

        </section>
    );
};

export default ParallaxPhoto;
