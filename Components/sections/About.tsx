import React from "react";
import Image from "next/image";
import styles from "../../styles/About.module.scss";

interface PhotoProps {
    src: string;
    alt: string;
}

const PHOTOS: Array<PhotoProps> = [
    {
        src: '/robin/repair.jpg',
        alt: "Photo de Robin Lefèvre en train d'accorder un piano"
    },
    {
        src: '/deco/repair.jpg',
        alt: "Photo d'un piano en réparation"
    },
    {
        src: '/deco/workbench.jpg',
        alt: "Photo d'un atelier de réparation de piano"
    },
    {
        src: '/robin/concert.jpg',
        alt: "Photo de Robin Lefèvre en train de réparer un piano dans une salle de concert"
    }
]

const About: React.FC = () => {
    return (
        <section data-section="a-propos" className={`${styles.section} text-black px-6 pt-7 pb-24 lg:pb-9 lg:px-8`}>
            <span className={styles.anchor} id="a-propos"></span>

            <div
                className="mx-auto grid max-w-2xl grid-cols-1 items-center gap-x-8 gap-y-8 lg:max-w-7xl lg:grid-cols-2">

                <div className="leading-7">
                    <h2 className="text-3xl font-bold tracking-tight lg:text-4xl">
                        Qui je suis
                    </h2>
                    <p className="mt-8">
                        J’ai toujours été un grand amoureux de la musique et dévoué à celle-ci depuis mon plus jeune
                        âge. J’y ai consacré la plupart de mon temps au cours de ces vingt dernières années.
                    </p>
                    <p className="mt-4">
                        Grâce à mes parents, j’ai baigné dans un univers riche de rock britannique des années 70 à
                        aujourd’hui : les Pink Floyd, Led Zeppelin, les Beatles, Radiohead... Et c’est très logiquement
                        que j’ai ressenti le besoin de jouer ce que j’écoutais pour pouvoir en jouir pleinement.
                    </p>
                    <p className="mt-4">
                        J’ai commencé par le piano très tôt à l’âge de sept ans. Et ensuite, j’ai voulu me délecter
                        d’autres sonorités comme la guitare, la batterie, le chant... pour pouvoir me plonger encore plus
                        dans cet univers.
                    </p>
                    <p className="mt-4">
                        A 20 ans, je reviens à mon premier instrument : le piano. Je décide de me lancer dans la
                        réparation et l’entretien de nos merveilleux pianos et j’intègre l’<abbr title="Institut
                        Technologique Européens des Métiers de la Musique">ITEMM</abbr> en 2014. J’obtiens avec fierté
                        mon Brevet des Métiers d’Arts technicien pianos en 2018. Je peux dès lors concilier mon travail
                        d’accordeur avec la passion que j’ai pour la musique.
                    </p>
                    <p className="mt-4">
                        Au cours des dix dernières années, j’ai bâti de solides compétences et développé un réel amour
                        pour la réparation et l’accord des pianos, si bien que j’ai pu avoir l’honneur et la fierté de
                        travailler pour de prestigieuses salles comme celle de <i>l’Olympia</i>, <i>La Salle Pleyel </i>
                        ou encore la <i>Salle Colonne</i>.
                    </p>
                    <p className="mt-4">
                        C’est donc avec joie et diligence que je me tiens prêt à combler tous vos besoins pianistiques.
                        Puisse ma sensibilité et mon écoute répondre à vos désirs de musique dans votre vie.
                    </p>
                    <p className="mt-4">
                        Robin
                    </p>
                </div>

                <div className="grid grid-cols-2 grid-rows-2 gap-4 sm:gap-6 lg:gap-8">

                    {PHOTOS.map((photo: PhotoProps, index: number) => {
                        return (
                            <figure key={index}
                                    className={`flex ${index % 2 === 0 ? 'justify-end relative top-14' : ''}`}>
                                <Image src={photo.src} alt={photo.alt} width={500} height={0}
                                       className="object-cover w-60 h-60 rounded-lg"/>
                            </figure>
                        )
                    })}

                </div>

            </div>

        </section>
    );
};

export default About;
