import React from "react";
import styles from "../../styles/Testimonies.module.scss";
import Slider from "react-slick";
import { SlickParameters } from "../../interfaces/slick-parameters";

interface TestimonyProps {
    quote: string,
    author: string,
    city: string
}

const TESTIMONIES: Array<TestimonyProps> = [
    {
        quote: 'Je tiens à vous remercier de la grande qualité de votre savoir-faire. Vous avez procédé avec efficacité et professionnalisme à l’harmonisation, au réglage et à l’accord de mon piano.',
        author: 'Mme MERLE-RAYMOND',
        city: 'Orgerus'
    },
    {
        quote: 'Nous sommes très reconnaissants pour tout le travail effectué sur notre vieux piano qui n’avait pas vu d’accordeur depuis plus de vingt ans… Nous sommes impressionnés par le professionnalisme de Robin, allié à un art de la pédagogie si bienveillante. Que dire aussi de son perfectionnisme qui ne ménage ni son temps ni sa peine. Bravo et merci encore.',
        author: 'M. et Mme KLEINBERGER',
        city: 'Longnes'
    },
    {
        quote: 'Un grand merci pour le travail réalisé sur le piano. Le toucher est beaucoup plus doux et mélodieux. C’est beaucoup plus simple pour moi de jouer… quel plaisir… à bientôt.',
        author: 'Cécile C.',
        city: 'Pierrelaye'
    },
    {
        quote: 'Cher Robin, je suis très heureux que vous ayez pris grand soin de mon piano, cet après-midi, lui et moi vous en remercions chaleureusement.\n',
        author: 'François G.',
        city: 'Paris'
    }
];

const PARAMETERS: SlickParameters = {
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 1500,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 15000,
    fade: true,
    pauseOnHover: false,
    draggable: false,
    adaptiveHeight: true
};

const Testimonies: React.FC = () => {
    return (
        <section data-section="temoignages"
            className={`${styles.section} ${styles.testimonies} relative isolate overflow-hidden px-6 pt-7 pb-9 lg:px-0`}>
            <span className={styles.anchor} id="temoignages"></span>

            <div className="mx-auto max-w-2xl text-center">
                <h2 className="mt-2 text-3xl font-bold tracking-tight lg:text-4xl">
                    Témoignages
                </h2>
            </div>

            <Slider {...PARAMETERS} className={styles.slickSlider}>
                {TESTIMONIES.map((testimony: TestimonyProps, index: React.Key) => (
                    <figure key={index} className={`${styles.testimony} mt-10 mx-auto max-w-2xl lg:max-w-4xl`}>
                        <blockquote
                            className="text-center text-xl font-semibold leading-8 lg:text-2xl lg:leading-9">
                            <p>
                                {testimony.quote}
                            </p>
                        </blockquote>
                        <figcaption className="mt-10">
                            <div className="mt-4 flex items-center justify-center space-x-3 text-base">
                                <div className="font-semibold">{testimony.author}</div>
                                <svg viewBox="0 0 2 2" width={3} height={3} aria-hidden="true"
                                     className="fill-gray-900">
                                    <circle cx={1} cy={1} r={1}/>
                                </svg>
                                <div>{testimony.city}</div>
                            </div>
                        </figcaption>
                    </figure>
                ))}
            </Slider>

        </section>
    );
};

export default Testimonies;
