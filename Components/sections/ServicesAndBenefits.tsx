import React from "react";
import {CogIcon, BellAlertIcon, ShoppingCartIcon, WrenchScrewdriverIcon} from '@heroicons/react/24/outline'
import styles from "../../styles/ServicesAndBenefits.module.scss";
import Link from "next/link";
import {IoDiamondOutline} from 'react-icons/io5';

interface ServiceProps {
    name: string,
    description: string,
    asterisk?: string,
    icon: any
}

const SERVICES: Array<ServiceProps> = [
    {
        name: 'Accord',
        description: 'L’accord est indispensable au bon entretien de votre piano et pour le ' +
            'confort de vos oreilles. Il est recommandé de le faire chaque année. ' +
            'Cette étape prend en moyenne 1h30 et consiste à remettre les ' +
            'cordes du piano juste par rapport à un diapason donné.',
        icon: BellAlertIcon,
    },
    {
        name: 'Réglage',
        description: 'Pour répondre à toutes vos exigences pianistiques, un bon réglage permet d’optimiser votre ' +
            'instrument et d’avoir plus de contrôle sous les doigts. Fluidifiez le clavier et donnez plus de ' +
            'répétition à la mécanique.',
        icon: CogIcon,
    },
    {
        name: 'Harmonisation',
        description: 'Pour sublimer le son de votre piano et parfaire le timbre* de votre ' +
            'instrument en fonction de ses besoins ou de vos désirs : son trop ' +
            "métallique ou trop sourd, ...",
        asterisk: "* Le timbre désigne la couleur du son",
        icon: IoDiamondOutline,
    },
    {
        name: 'Expertise et réparation',
        description:
            'Quelle est la santé de votre piano et combien vaut-il ? Peut-on le remettre en état et est-ce pertinent ? ' +
            'Quelles réparations sont nécessaires à son bon fonctionnement ? ' +
            'Obtenez un diagnostic précis et détaillé de votre instrument pour vous aider à savoir quoi en faire.',
        icon: WrenchScrewdriverIcon,
    },
    {
        name: 'Achat et revente',
        description: 'Trouver votre piano idéal. ' +
            'Ou bien vous avez un instrument et souhaitez le revendre ?',
        icon: ShoppingCartIcon,
    },
];

const ServicesAndBenefits: React.FC = () => {
    return (
        <section data-section="services-et-prestations"
                 className={`${styles.section} ${styles.service} px-6 pt-7 pb-9 lg:px-8`}>
            <span className={styles.anchor} id="services-et-prestations"></span>

            <div className="mx-auto max-w-7xl px-6 lg:px-8">

                <div className="mx-auto max-w-2xl lg:text-center">
                    <h2 className="mt-2 text-3xl font-bold tracking-tight lg:text-4xl">
                        Services & prestations
                    </h2>
                </div>

                <div className="mx-auto mt-12 max-w-2xl lg:max-w-4xl">
                    <div className="grid max-w-xl grid-cols-1 gap-x-8 gap-y-10 lg:max-w-none lg:grid-cols-2 lg:gap-y-16">
                        {SERVICES.map((service: ServiceProps) => (
                            <div key={service.name} className="relative pl-16">
                                <div className="text-base font-semibold leading-7 pb-2">
                                    <div
                                        className={`${styles.icon} absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg`}>
                                        <service.icon className="h-6 w-6 text-white" aria-hidden="true"/>
                                    </div>
                                    <h3>{service.name}</h3>
                                </div>
                                <div className="mx-2 text-base leading-7">
                                    {service.description.split(/\n/).map((line: string, index: React.Key) =>
                                        <p key={index}>{line}</p>)
                                    }
                                    {service.asterisk && (
                                        <i className="text-xs">{service.asterisk}</i>
                                    )}
                                </div>
                            </div>
                        ))}
                    </div>
                </div>

                <div className="mt-12 flex justify-center">
                    <Link className="text-white button rounded-lg py-2 px-4 border" href="/#contact">
                        Me contacter
                    </Link>
                </div>

            </div>

        </section>
    );
};

export default ServicesAndBenefits;
