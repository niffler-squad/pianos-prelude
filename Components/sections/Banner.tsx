import React from "react";
import {Parallax} from 'react-scroll-parallax';
import Image from "next/image";
import decoBanner from '../../public/deco/banner.jpg';
import styles from "../../styles/Banner.module.scss";

const Banner: React.FC = () => {
    return (
        <section data-section="hero-banner"
            className={`${styles.section} ${styles.heroHeader} relative isolate overflow-hidden text-white`}>
            <span className={styles.anchor} id="hero-banner"></span>

            <Parallax translateY={[-20, 20]}>
                <figure className="flex items-center justify-center">
                    <Image
                        className="absolute w-full object-center"
                        src={decoBanner} alt="Photo de piano" width={3000} height={432} priority={true}/>
                </figure>
            </Parallax>

            <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 w-full px-6 lg:px-8">
                <div>

                    <h1 className="text-4xl text-center lg:text-6xl">
                        PIANOS PRÉLUDE
                    </h1>

                    <span className="block text-2xl text-center lg:text-3xl">
                        au service de votre musique
                    </span>

                    <h2 className="mt-6 text-center italic">
                        Robin Lefèvre, technicien accordeur en région parisienne.
                    </h2>

                </div>
            </div>

        </section>
    );
};

export default Banner;
