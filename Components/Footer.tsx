import React from "react";
import {Disclosure} from "@headlessui/react";
import styles from "../styles/Footer.module.scss";
import Image from "next/image";
import Link from "next/link";
import {GiRaccoonHead} from 'react-icons/gi';
import {IoPaw} from 'react-icons/io5';

interface SocialMediaProps {
    name: string,
    image: string,
    value: string,
    url: string,
}

const SOCIAL_MEDIAS: Array<SocialMediaProps> = [
    {
        name: 'instagram',
        image: 'instagram.svg',
        value: 'robinpiano_',
        url: 'https://www.instagram.com/robinpiano_/',
    },
    {
        name: 'facebook',
        image: 'facebook.svg',
        value: 'PianosPrélude - RobinLefèvre',
        url: 'https://www.facebook.com/pianospreluderobinlefevre',
    },
    {
        name: 'enveloppe',
        image: 'envelope.svg',
        value: 'pianosprelude@gmail.com',
        url: 'mailto:pianosprelude@gmail.com',
    },
    {
        name: 'téléphone',
        image: 'phone.svg',
        value: '0612611425',
        url: 'tel:0612611425',
    },
]

const Footer: React.FC = () => {
    const YEAR: number = new Date().getFullYear();

    return (
        <Disclosure as="footer" className={`${styles.footer} px-4 pt-6 text-white lg:px-0`}>
            <nav className="mx-auto max-w-7xl">

                <ul className="flex justify-center gap-20">
                    {SOCIAL_MEDIAS.map((socialMedia: SocialMediaProps, index: React.Key) => (
                        <li key={index}>
                            <Link href={socialMedia.url} className="flex gap-4" target="_blank">
                                <Image src={socialMedia.image} alt={`Logo de ${socialMedia.name}`}
                                       width={30} height={30}/>
                                <span className="hidden lg:block">{socialMedia.value}</span>
                            </Link>
                        </li>
                    ))}
                </ul>

            </nav>
            <div className="py-4 text-center text-sm">
                {`Pianos Prelude © Tous droits réservés - ${YEAR} - Graphisme et Développement : `}

                <IoPaw className="inline-block h-4 w-4"/> &nbsp;
                <a href="https://fr.linkedin.com/in/marionviault" target="_blank" className="underline hover:no-underline">
                    Marion Viault
                </a>
                &nbsp; & &nbsp;
                <GiRaccoonHead className="inline-block h-4 w-4"/>
            </div>
        </Disclosure>
    );
};

export default Footer;
