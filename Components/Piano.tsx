import React, {useState} from "react";
import {PianoProps} from "../interfaces/piano";
import {SlickParameters} from "../interfaces/slick-parameters";
import Image from "next/image";
import Slider from "react-slick";
import {ChevronUpIcon} from "@heroicons/react/24/outline";
import Arrow from "./Arrow";

const PARAMETERS: SlickParameters = {
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    prevArrow: <Arrow position="left-2"/>,
    nextArrow: <Arrow position="right-2"/>
}

const Piano: React.FC<PianoProps> = (piano: PianoProps) => {

    const PHOTOS = piano.photos;

    const [moreInfosVisibility, setMoreInfosVisibility] = useState(false);

    const handleMoreInfosVisibility = () => {
        setMoreInfosVisibility((current) => !current);
    };

    return (
        <div className="group relative text-white">

            <figure className="relative overflow-hidden rounded-md lg:h-80">
                {piano.sold ? <span
                    className="absolute z-10 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-6xl rotate-12">VENDU</span> : ''}
                {PHOTOS.length > 1 ?
                    <Slider {...PARAMETERS}
                            className="bg-black h-full w-full object-cover object-center lg:h-full lg:w-full">
                        {PHOTOS.map((photo, index: React.Key) => {
                            return (
                                <Image key={index} src={photo.src} alt={`Photo du ${piano.model}`} width={405}
                                       height={0} className={`${piano.sold ? 'opacity-50' : ''} object-cover h-80`}/>
                            )
                        })}
                    </Slider>
                    :
                    <div className="bg-black h-full w-full object-cover object-center lg:h-full lg:w-full">
                        <Image src={PHOTOS[0].src} alt={`Photo du ${piano.model}`} width={405}
                               height={0} className={`${piano.sold ? 'opacity-50' : ''} object-cover h-80 w-full`}/>
                    </div>

                }
            </figure>

            <div className="absolute bottom-0 w-full z-10 bg-black rounded-b-md opacity-80 px-2 pb-2">

                <ChevronUpIcon className={`content-center h-4 w-full ${moreInfosVisibility ? 'rotate-180' : ''}`}
                               onClick={handleMoreInfosVisibility}/>

                <div className="flex justify-between font-medium">
                    <h3><strong>{piano.brand} - {piano.model}</strong></h3>
                    <strong>{piano.price} €</strong>
                </div>

                {moreInfosVisibility && (

                    <div className="leading-6 text-sm mt-3">

                        <p className="flex justify-between">
                            <span><strong>Année</strong> : {piano.year}</span>
                            <span><strong>Finition</strong> : {piano.color}</span>
                        </p>

                        <p className="flex justify-between">
                            <span><strong>Fabrication</strong> : {piano.manufacturing}</span>
                            <span><strong>Dimensions</strong> : {piano.size}</span>
                        </p>

                        <p className="mt-3">{piano.description}</p>
                    </div>

                )}

            </div>

        </div>
    );
};

export default Piano;
