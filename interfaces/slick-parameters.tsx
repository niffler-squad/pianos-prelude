export interface SlickParameters {
    slidesToShow: number;
    slidesToScroll: number;
    speed: number;
    arrows?: boolean;
    prevArrow?: object;
    nextArrow?: object;
    autoplay?: boolean;
    autoplaySpeed?: number;
    fade?: boolean;
    pauseOnHover?: boolean;
    draggable?: boolean;
    adaptiveHeight?: boolean;
}