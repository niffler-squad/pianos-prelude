import React from "react";
import { ParallaxProvider } from 'react-scroll-parallax';
import Banner from "../Components/sections/Banner";
import About from "../Components/sections/About";
import ServicesAndBenefits from "../Components/sections/ServicesAndBenefits";
import Reinsurance from "../Components/sections/Reinsurance";
import PianosForSale from "../Components/sections/PianosForSale";
import Testimonies from "../Components/sections/Testimonies";
import Contact from "../Components/sections/Contact";
import Partners from "../Components/sections/Partners";
import ParallaxPhoto from "../Components/sections/ParallaxPhoto";
import parallaxPhoto1 from '../public/deco/photo-section-1.jpg';
import parallaxPhoto2 from '../public/deco/photo-section-2.jpg';
import parallaxPhoto3 from '../public/deco/photo-section-3.jpg';

const Home: React.FC = () => {

    return (
        <ParallaxProvider>
            <Banner/>
            <About/>
            <ParallaxPhoto parallaxSrc={parallaxPhoto1.src}/>
            <ServicesAndBenefits/>
            <Reinsurance/>
            <ParallaxPhoto parallaxSrc={parallaxPhoto2.src}/>
            <PianosForSale/>
            <Testimonies/>
            <ParallaxPhoto parallaxSrc={parallaxPhoto3.src}/>
            <Contact/>
            <Partners/>
        </ParallaxProvider>
    )
};

export default Home;
