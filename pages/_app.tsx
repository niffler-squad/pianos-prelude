import './../styles/globals.scss'
import styles from "../styles/variables.module.scss";
import Navigation from "./../Components/Navigation";
import Head from "next/head";
import Footer from "./../Components/Footer";
import React, { useEffect, useRef, useState } from "react";
import { AppProps } from "next/app";
// @ts-ignore
import { NextFont } from "next/font";
import { Playfair_Display } from "next/font/google";

export const FONT_GOOGLE: NextFont = Playfair_Display({
    subsets: [ 'latin' ],
    display: 'swap',
    variable: '--font-google',
    weight: [ '500', '700' ],
});

function PianoApp({ Component, pageProps }: AppProps) {

    const [ activeSection, setActiveSection ] = useState(null);
    const SECTIONS = useRef<Array<HTMLElement>>([]);

    const handleScroll = () => {
        const PAGE_Y_OFFSET: number = window.scrollY;
        let newActiveSection: string = null;
        const THRESHOLD: number = 300;

        SECTIONS.current.forEach((section) => {
            const SECTION_OFFSET_TOP: number = section.offsetTop - THRESHOLD;
            const SECTION_HEIGHT: number = section.offsetHeight;

            if (PAGE_Y_OFFSET >= SECTION_OFFSET_TOP && PAGE_Y_OFFSET < SECTION_OFFSET_TOP + SECTION_HEIGHT) {
                newActiveSection = section.getAttribute('data-section');
            }
        });

        setActiveSection(newActiveSection);

    };

    useEffect(() => {
        // @ts-ignore
        SECTIONS.current = document.querySelectorAll('[data-section]');
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        }
    }, []);

    return (
        <>

            <Head>
                <title>Pianos Prélude, au service de votre musique - Robin Lefèvre</title>
                <meta name="description" content="Robin Lefèvre, technicien accordeur de piano en région parisienne.
                Accord, réglage, harmonisation, expertise, réparation, achat et revente."/>
                <meta name="keywords"
                      content="piano pianos pianno piannos technicien accordeur accordage accordement paris région
                      parisienne ile de france Seine-Saint-Denis Val-de-Marne Hauts-de-Seine Yvelines Essonne
                      Seine-et-Marne Val d'Oise 75 92 93 94 95 78 91 77 musique studio instrument accord harmonisation
                      expertise réparation achat vente Young Chang Schimmel Yamaha Petrof"/>
                <link rel="icon" href="/favicon.png"/>
            </Head>

            <div className={`${FONT_GOOGLE.variable} ${styles.div} min-h-screen`}>

                <main>
                    <Navigation activeSection={activeSection}/>
                    <Component {...pageProps} />
                </main>

                <Footer/>

            </div>

        </>
    )
}

export default PianoApp;
